import Vue from "vue";
import VueMask from "v-mask";
import VueI18n from "vue-i18n";



import VueRouter from "vue-router";
import VueResource from "vue-resource";
import VueNotifications from "vue-notifications";
import miniToastr from "mini-toastr";
import App from "./App.vue";
import Ads from "./Ads.vue";
import Welcome from "./Welcome.vue";
import AboutUs from "./AboutUs.vue";
import Help from "./Help.vue";
import SignIn from "./SignIn.vue";
import ResetPassword from "./ResetPassword.vue";
import SignUp from "./SignUp.vue";
import News from "./News.vue";
import NewsFetch from "./NewsFetch.vue";
import Orders from "./Orders.vue";
import Balans from "./Balans.vue";
import Notifications from "./Notifications.vue";
import Calculator from "./Calculator.vue";
import Settings from "./Settings.vue";
import AddOrder from "./AddOrder.vue";
import OrderFetch from "./OrderFetch.vue";
import OrderEdit from "./OrderFetch.vue";
import OrderDelete from "./OrderFetch.vue";
import ChangeLang from "./ChangeLang.vue";


import "./assets/css/bootstrap.min.css";
import "./assets/css/style.css";

Vue.component('ads', Ads)
Vue.use(VueI18n)
Vue.use(VueRouter);
Vue.use(VueResource);
Vue.use(VueMask);




const routes = [
    {path: '/', component: Welcome},
    {path: '/signIn', component: SignIn, name: 'signIn'},
    {path: '/signUp', component: SignUp},
    {path: '/resetPassword', component: ResetPassword},
    {path: '/aboutUS', component: AboutUs},
    {path: '/help', component: Help},
    {path: '/news', component: News},
    {path: '/changeLang/:lang', component: ChangeLang, name: 'ChangeLang', props: true},
    {path: '/news/:id', component: NewsFetch, props: true},
    {path: '/cabinet/orders', component: Orders, name: 'myOrders'},
    {path: '/cabinet/orders/fetch/:id', component: OrderFetch, name: 'OrderFetch', props: true},
    {path: '/cabinet/orders/edit/:id', component: OrderEdit, props: true},
    {path: '/cabinet/orders/delete/:id', component: OrderDelete, props: true},
    {path: '/cabinet/orders/add', component: AddOrder},
    {path: '/cabinet/balans', component: Balans},
    {path: '/cabinet/notifications', component: Notifications},
    {path: '/cabinet/calculator', component: Calculator},
    {path: '/cabinet/settings', component: Settings},
];

const router = new VueRouter({
    routes
});

const toastTypes = {
    success: 'success',
    error: 'error',
    info: 'info',
    warn: 'warn'
}

miniToastr.init({types: toastTypes})

// Here we setup messages output to `mini-toastr`
function toast({title, message, type, timeout, cb}) {
    return miniToastr[type](message, title, timeout, cb)
}

// Binding for methods .success(), .error() and etc. You can specify and map your own methods here.
// Required to pipe our output to UI library (mini-toastr in example here)
// All not-specified events (types) would be piped to output in console.
const options = {
    success: toast,
    error: toast,
    info: toast,
    warn: toast
}

// Activate plugin
Vue.use(VueNotifications, options)

/*
 if (localStorage.getItem('token'))  {
 let api = require('./api.js');
 Vue.http.post(api.apiResource + 'customer/account/fetch', {
 'token': localStorage.getItem('token')
 }).then(response => {
 if (response.body.status == 'success')
 {
 localStorage.getItem('user', response.body.result)
 }
 }, response => {
 console.debug(response.body);
 });

 }*/

const messages = {
    en: {
        links: {
            help: 'Help',
            signIn: 'Sign In',
            signUp: 'Sign Up',
            aboutUs: 'About company',
            news: 'News',
            cabinet: 'Profile',
            myOrders: 'My orders',
            balance: 'Balance',
            calc: 'Calculator',
            settings: 'Settings',
        },
        helpers: {
            change_lang: 'Changing language',
        },
        index: {
            addOrder: 'Add cargo',
            slogan: 'Cargoes and transports',
            underSlogan: 'Convenient and high-quality information service in the field of road freight. We provide only quality information on direct freight customers and freight orders',

        },
        orders: {
            all: 'All',
            inModeration: 'Moderation',
            pending: 'Pending',
            accepted: 'Accepted',
            canceled: 'Refusals',
            delivered: 'Delivered',
            finished: 'Completed',
            tg: 'kzt',
            add: 'Add load',
            noExecuters: 'Nobody executes',
        },
        account: {
            settings: 'Settings',
            urData: 'Your data',
            photo: 'Photo',
            photoUpload: 'Upload',
            username: 'Name of organization / individual',
            iin: 'BIN / IIN',
            sphere: 'Sphere',
            phone: 'Phone',
            legal_address: 'Legal address',
            actual_address: 'Actual address',
            save: 'Save',
            changePassword: 'Change password',
            oldPassword: 'Old password',
            newPassword: 'New password',
            leaveAccount: 'Sign out of your account',
            exitAccount: 'Exiting the profile',
        },
        calc: {
            from: 'From',
            to: 'To',
            city: 'City...',
            calculate: 'Calculate',
            distance: 'Distance',
            deliveryTime: 'Time',
            minute: 'min',
        },
        order: {
            completeOrder: 'Complete order',
            findLocation: 'Find out the location',
            edit: 'Edit',
            delete: 'Delete',
            from: 'Point of departure',
            to: 'Destination',
            order: 'Order',
            capacity: 'Capacity',
            volume: 'Volume',
            insurance: 'Insurance',
            insured: 'Insured',
            notInsured: 'Not insured',
            description: 'Description',
            preferredCar: 'Preferred transport',
            typeTransp: 'Type of shipment',
            transpView: 'Type of transport',
            provisionalDate: 'Preliminary date of shipment',
            meters: 'Distance along the route',
            time: 'Time of transportation',
            hours: 'h.',
            minutes: 'm.',
            km: 'km',
            meter: 'm',
            mt: 'mt',
            price: 'Cost of transportation',
            transporter: 'Executor',
            transporterCar: 'Provider transport',
            using: 'Uploaded',
            free: 'Free',
            viewTransp: 'Type of transport',
            hazardClass: 'Hazard class',
            copy: 'Copy data from previous orders',
            selectOrder: 'Select an order',
            sendTo: 'Where to send?',
            whatLoad: 'What load?',
            labelPreferredCar: 'What transport do you prefer?',
            extraOptions: 'Extra options',
            labelPayment: 'Terms of payment',
            loadingOption: 'Loading option',
            dateShipment: 'Date of shipment',
            helperText: 'Cargo by distance',
            tg: 'tg',
        },
    },
    ru: {
        links: {
            help: 'Помощь',
            signIn: 'Вход',
            signUp: 'Регистрация',
            aboutUs: 'О компании',
            news: 'Новости',
            cabinet: 'Кабинет',
            myOrders: 'Мои грузы',
            balance: 'Мой баланс',
            calc: 'Калькулятор',
            settings: 'Настройки',
        },
        helpers: {
            change_lang: 'Смена языка',
        },
        index: {
            addOrder: 'Добавить груз',
            slogan: 'Грузы и транспорты',
            underSlogan: 'Удобный и качественный информационный сервис в сфере автомобильных грузоперевозок. Мы предоставляем исключительно качественную информацию о прямых Заказчиках грузовых перевозок и заказы на грузоперевозки',
        },
        orders: {
            all: 'Все',
            inModeration: 'В модерации',
            pending: 'В ожидании',
            accepted: 'Принятые',
            canceled: 'Отказанные',
            delivered: 'Доставленные',
            finished: 'Завершенные',
            tg: 'тг',
            add: 'Добавить',
            noExecuters: 'Нет исполнителя',
        },
        account: {
            settings: 'Настройки',
            urData: 'Ваши данные',
            photo: 'Фотография',
            photoUpload: 'Загрузить',
            username: 'Наименование организации/ФЛ',
            iin: 'БИН/ИИН',
            sphere: 'Сфера деятельности',
            phone: 'Номер телефона',
            legal_address: 'Юридический адрес',
            actual_address: 'Фактический адрес',
            save: 'Сохранить',
            changePassword: 'Сменить пароль',
            oldPassword: 'Старый пароль',
            newPassword: 'Новый пароль',
            leaveAccount: 'Выйти из аккаунта',
            exitAccount: 'Выход из аккаунта',
        },
        calc: {
            from: 'Откуда',
            to: 'Куда',
            city: 'Город...',
            calculate: 'Рассчитать',
            distance: 'Расстояние',
            deliveryTime: 'Время в пути',
            minute: 'мин',
        },
        order: {
            completeOrder: 'Завершить заказ',
            findLocation: 'Узнать местоположение',
            edit: 'Редактировать',
            delete: 'Удалить',
            from: 'Пункт отправления',
            to: 'Пункт назначения',
            order: 'Груз',
            capacity: 'Объем',
            volume: 'Вес',
            insurance: 'Страховка',
            insured: 'Застрахован',
            notInsured: 'Не застрахован',
            description: 'Описание',
            preferredCar: 'Предпочитаемый транспорт',
            typeTransp: 'Тип перевозки',
            transpView: 'Вид транпорта',
            provisionalDate: 'Предварительная дата погрузки',
            meters: 'Расстояние по маршруту',
            time: 'Время перевозки',
            hours: 'h.',
            minutes: 'm.',
            km: 'км',
            meter: 'm',
            mt: 'т',
            price: 'Стоимость перевозки',
            transporter: 'Исполнитель',
            transporterCar: 'Транспорт иcполнителя',
            using: 'Загружено',
            free: 'Свободно',
            viewTransp: 'Вид транспорта',
            hazardClass: 'Класс опасности',
            copy: 'Скопировать данные из предыдущих заказов',
            selectOrder: 'Выберите заказ',
            sendTo: 'Куда отправить?',
            whatLoad: 'Какой груз?',
            labelPreferredCar: 'Какой транспорт предпочитаете?',
            extraOptions: 'Дополнительные параметры',
            labelPayment: 'Условия оплаты',
            loadingOption: 'Вариант погрузки',
            dateShipment: 'Предварительная дата погрузки',
            helperText: 'груза на расстояние',
            tg: 'тг',
        },
    },
    kk: {
        links: {
            help: 'Көмек',
            signIn: 'Кіру',
            signUp: 'Тіркелу',
            aboutUs: 'Біз Туралы',
            news: 'Жаңалықтар',
            cabinet: 'Профиль',
            myOrders: 'Менің тапсырыстарым',
            balance: 'Баланс',
            calc: 'Калькулятор',
            settings: 'Баптаулар',
        },
        helpers: {
            change_lang: 'Тілді өзгерту',
        },
        index: {
            addOrder: 'Жүк енгізу',
            slogan: 'Жүк тасымалдаушылар',
            underSlogan: 'Жүк тасымалдау саласындағы ыңғайлы және жоғары сапалы ақпараттық қызмет көрсету. Біз тікелей клиенттер және жүк тасымалдаушылар арасында сапалы қызмет көрсетеміз',

        },
        orders: {
            all: 'Барлығы',
            inModeration: 'Тексеруде',
            pending: 'Күтуде',
            accepted: 'Қабылданды',
            canceled: 'Бас тартылды',
            delivered: 'Жеткізілді',
            finished: 'Аяқталды',
            tg: 'kzt',
            add: 'Жүк енгізу',
            noExecuters: 'Ешкім орындамады',
        },
        account: {
            settings: 'Баптаулар',
            urData: 'Мерзімі',
            photo: 'Фото',
            photoUpload: 'Жүктеу',
            username: 'Ұйымның атауы / ЖСИН: BIN / IIN',
            sphere: 'Sphere',
            phone: 'Телефон',
            legal_address: 'Заңды мекен жайы',
            actual_address: 'Нағыз мекен жайы',
            save: 'Сақтау',
            changePassword: 'Құпия сөзді өзгерту',
            oldPassword: 'Ескі құпия сөз',
            newPassword: 'Жаңа құпия сөз',
            leaveAccount: 'Тіркелуден шығу',
            exitAccount: 'Профильден шығу',
        },
        calc: {
            from: 'Бастап',
            to: 'дейін',
            city: 'Қала…',
            calculate: 'Есептеу',
            distance: 'Қашықтық',
            deliveryTime: 'Уақыт',
            minute: 'мин',
        },
        order: {
            completeOrder: 'Тапсырыс орындалды',
            findLocation: 'Орналасқан жерін табу',
            edit: 'Өзгерту',
            delete: 'Өшіру',
            from: 'Жөнелтілу',
            to: 'Жеткізу',
            order: 'Тапсырыс',
            capacity: 'Сыйымдылығы',
            volume: 'Көлемі',
            insurance: 'Сақтандыру',
            insured: 'Сақтандырылған',
            notInsured: 'Сақтандырылмаған',
            description: 'Сипаттамасы',
            preferredCar: 'Қалаған көлік',
            typeTransp: 'Тиеу типі',
            transpView: 'Транспорт типа',
            provisionalDate: 'Алдын-ала жөнелтілу күні',
            meters: 'Арақашықтық',
            time: 'Тасымалдау уақыты',
            hours: 'с.',
            minutes: 'мин.',
            km: 'км',
            meter: 'м',
            mt: 'mt',
            price: 'Тасымалдау бағасы',
            transporter: 'Орындаушы',
            transporterCar: 'Провайдер көлік',
            using: 'Жүктелді',
            free: 'Тегін',
            viewTransp: 'Транспорт типа',
            hazardClass: 'Қауіптілік класы',
            copy: 'Алдыңғы тапсырыстарды деректерді көшіру',
            selectOrder: 'Ретін таңдаңыз',
            sendTo: 'Қайда жіберу?',
            whatLoad: 'Қандай жүктеме?',
            labelPreferredCar: 'Қандай көлік қалайсыз?',
            extraOptions: 'Толығырақ iздеу параметрлері',
            labelPayment: 'Төлем шарттары',
            loadingOption: 'Тиеу опция',
            dateShipment: 'Алдын-ала тиеу күні',
            helperText: 'қашықтықта жүк',
            tg: 'тг',
        },
    },
}


const i18n = new VueI18n({
    locale: 'en', // set locale
    messages, // set locale messages
})
Vue.prototype.$locale = {
    change (language) {
        i18n.locale = language;
    },
    current () {
        return i18n.locale;
    }
};

let vm = new Vue({
    el: '#app',
    router,
    i18n,
    render: h => h(App),
    components: {
        //'ads': ads
    }
});





